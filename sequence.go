package rosalind

import (
	"errors"
	"strings"
	"bytes"
)

type Sequence interface {
	String() string
	// Number of occurences of given character at the position within the
	// sequence. DNA would have map keys of 'A', 'C', 'G', 'T'. Given a DNA
	// sequence ACTGCCTA: map['A'][0] would be 1, map['C'][0] would be 0.
	ConsensusProfile() ConsensusProfile
}

type DNASequence string

func (d DNASequence) String() string {
	return string(d)
}

func (d DNASequence) ConsensusProfile() ConsensusProfile {
	profile := make(map[string]map[int]int)
	for i, n := range d.String() {
		if _, ok := profile[string(n)]; !ok {
			profile[string(n)] = make(map[int]int)
		}

		profile[string(n)][i] = 1
	}

	return profile
}

const STOP_CODON string = "."
const DNA_START_CODON string = "ATG"
var DNA_CODONS map[string]string = map[string]string{
	"TTT": "F",
	"TTC": "F",
	"TTA": "L",
	"TTG": "L",
	"TCT": "S",
	"TCC": "S",
	"TCA": "S",
	"TCG": "S",
	"TAT": "Y",
	"TAC": "Y",
	"TAA": STOP_CODON,
	"TAG": STOP_CODON,
	"TGT": "C",
	"TGC": "C",
	"TGA": STOP_CODON,
	"TGG": "W",
	"CTT": "L",
	"CTC": "L",
	"CTA": "L",
	"CTG": "L",
	"CCT": "P",
	"CCC": "P",
	"CCA": "P",
	"CCG": "P",
	"CAT": "H",
	"CAC": "H",
	"CAA": "Q",
	"CAG": "Q",
	"CGT": "R",
	"CGC": "R",
	"CGA": "R",
	"CGG": "R",
	"ATT": "I",
	"ATC": "I",
	"ATA": "I",
	"ATG": "M",
	"ACT": "T",
	"ACC": "T",
	"ACA": "T",
	"ACG": "T",
	"AAT": "N",
	"AAC": "N",
	"AAA": "K",
	"AAG": "K",
	"AGT": "S",
	"AGC": "S",
	"AGA": "R",
	"AGG": "R",
	"GTT": "V",
	"GTC": "V",
	"GTA": "V",
	"GTG": "V",
	"GCT": "A",
	"GCC": "A",
	"GCA": "A",
	"GCG": "A",
	"GAT": "D",
	"GAC": "D",
	"GAA": "E",
	"GAG": "E",
	"GGT": "G",
	"GGC": "G",
	"GGA": "G",
	"GGG": "G",
}

const RNA_START_CODON string = "AUG"
var RNA_CODONS map[string]string = map[string]string{
	"UUU": "F",
	"UUC": "F",
	"UUA": "L",
	"UUG": "L",
	"UCU": "S",
	"UCC": "S",
	"UCA": "S",
	"UCG": "S",
	"UAU": "Y",
	"UAC": "Y",
	"UAA": STOP_CODON,
	"UAG": STOP_CODON,
	"UGU": "C",
	"UGC": "C",
	"UGA": STOP_CODON,
	"UGG": "W",
	"CUU": "L",
	"CUC": "L",
	"CUA": "L",
	"CUG": "L",
	"CCU": "P",
	"CCC": "P",
	"CCA": "P",
	"CCG": "P",
	"CAU": "H",
	"CAC": "H",
	"CAA": "Q",
	"CAG": "Q",
	"CGU": "R",
	"CGC": "R",
	"CGA": "R",
	"CGG": "R",
	"AUU": "I",
	"AUC": "I",
	"AUA": "I",
	"AUG": "M",
	"ACU": "T",
	"ACC": "T",
	"ACA": "T",
	"ACG": "T",
	"AAU": "N",
	"AAC": "N",
	"AAA": "K",
	"AAG": "K",
	"AGU": "S",
	"AGC": "S",
	"AGA": "R",
	"AGG": "R",
	"GUU": "V",
	"GUC": "V",
	"GUA": "V",
	"GUG": "V",
	"GCU": "A",
	"GCC": "A",
	"GCA": "A",
	"GCG": "A",
	"GAU": "D",
	"GAC": "D",
	"GAA": "E",
	"GAG": "E",
	"GGU": "G",
	"GGC": "G",
	"GGA": "G",
	"GGG": "G",
}

type RNASequence string

func (r RNASequence) String() string {
	return string(r)
}

func (d *DNASequence) ToRNA() RNASequence {
	return RNASequence(strings.Replace(string(*d), "T", "U", -1))
}

func (d *DNASequence) ReverseComplement() DNASequence {
	n := len(*d)
	nucleotides := make([]rune, len(*d))
	for _, r := range *d {
        n--
        nt := Nucleotide(r)
        comp := nt.Complement()
        nucleotides[n] = comp.Rune()
    }
    return DNASequence(nucleotides[n:])
}

func (d *DNASequence) GCContent() float64 {
	counts := NucleotideCounts(string(*d))
	total := len(*d)
	gcCount := counts['G'] + counts['C']

	return (float64(gcCount) / float64(total)) * 100
}

func (r *RNASequence) ToDNA() DNASequence {
	return DNASequence(strings.Replace(string(*r), "U", "T", -1))
}

type ORF struct {
	ID int
	Start int
	Stop int
	Sequence DNASequence
}

func FindORFs(d DNASequence, shifted int) []ORF {
	orfs := make([]ORF, 0)
	s := d.String()

	for i := 3; i <= len(s); i += 3 {
		codon := s[i-3:i]
		protein, _ := DNA_CODONS[codon]
		if codon == DNA_START_CODON {
			orf := ORF{}
			orf.Start = i - 3
			orf.Stop = -1
			orfs = append(orfs, orf)
		} else if protein == STOP_CODON || i >= len(s) - shifted {
			for j := len(orfs) - 1; j >= 0; j-- {		
				if orfs[j].Stop != -1 {
					break
				}
				orfs[j].Stop = i
				orfs[j].Sequence = DNASequence(s[orfs[j].Start: orfs[j].Stop])
			}
		}
	}

	return orfs
}

func (d *DNASequence) ORFs() []ORF {
	orfs := make([]ORF, 0)
	ds := d.String()
	for i := 0; i < 3; i++ {
		results := FindORFs(DNASequence(ds[i:len(ds)]), i + 1)
		for _, r := range results {
			r.ID = i + 1
			orfs = append(orfs, r)
		}
	}

	rds := d.ReverseComplement().String()
	for i := 0; i < 3; i++ {
		results := FindORFs(DNASequence(rds[i:len(rds)]), i + 1)
		for _, r := range results {
			r.ID = (i+1) * -1
			orfs = append(orfs, r)
		}
	}
	return orfs
}

type ProteinSequence string

func (p ProteinSequence) String() string {
	return string(p)
}

func (p ProteinSequence) Mass() float64 {
	ps := p.String()
	mass := 0.0
	for _, r := range ps {
		protein := Protein(r)
		mass += protein.MonoisotopicMass()
	}

	return mass
}

func (d *DNASequence) ToProtein() ProteinSequence {
	var ps bytes.Buffer
	ds := d.String()

	for i := 3; i <= len(ds); i += 3 {
		codon := ds[i-3:i]
		protein, _ := DNA_CODONS[codon]
		if protein == STOP_CODON {
			break
		}

		ps.WriteString(protein)
	}

	return ProteinSequence(ps.String())
}

func (r *RNASequence) ToProtein() ProteinSequence {
	var ps bytes.Buffer
	rs := r.String()

	for i := 3; i <= len(rs); i += 3 {
		codon := rs[i-3:i]
		protein, _ := RNA_CODONS[codon]
		if protein == STOP_CODON {
			break
		}

		ps.WriteString(protein)
	}

	return ProteinSequence(ps.String())
}

func HammingDistance(a Sequence, b Sequence) (int, error) {
	seqA := []rune(a.String())
	seqB := []rune(b.String())
	difference := 0
	if len(seqA) != len(seqB) {
		return difference, errors.New("String lengths are not the same!")
	}

	for i, r := range seqA {
		if r != seqB[i] {
			difference++
		}
	}

	return difference, nil
}

func FindMotifPositions(a Sequence, b Sequence) ([]int, error) {
	seqA := a.String()
	seqB := b.String()
	positions := make([]int, 0)

	if len(seqB) > len(seqA) {
		return positions, errors.New("Motif cannot be longer than given search sequence!")
	}

	for start := 0; start < len(seqA); start++ {
		end := start + len(seqB)
		if end > len(seqA) {
			break
		}

		sub := seqA[start:end]
		if sub == seqB {
			positions = append(positions, start+1)
		}
	}

	return positions, nil
}