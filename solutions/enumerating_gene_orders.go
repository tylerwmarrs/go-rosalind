package main

import (
	"fmt"
)

/*

Rearrangements Power Large-Scale Genomic Changesclick to collapse

Point mutations can create changes in populations of organisms from the same species, but they lack the power to create and differentiate entire species. This more arduous work is left to larger mutations called genome rearrangements, which move around huge blocks of DNA. Rearrangements cause major genomic change, and most rearrangements are fatal or seriously damaging to the mutated cell and its descendants (many cancers derive from rearrangements). For this reason, rearrangements that come to influence the genome of an entire species are very rare.

Because rearrangements that affect species evolution occur infrequently, two closely related species will have very similar genomes. Thus, to simplify comparison of two such genomes, researchers first identify similar intervals of DNA from the species, called synteny blocks; over time, rearrangements have created these synteny blocks and heaved them around across the two genomes (often separating blocks onto different chromosomes, see Figure 1.).

A pair of synteny blocks from two different species are not strictly identical (they are separated by the action of point mutations or very small rearrangements), but for the sake of studying large-scale rearrangements, we consider them to be equivalent. As a result, we can label each synteny block with a positive integer; when comparing two species' genomes/chromosomes, we then only need to specify the order of its numbered synteny blocks.

Problem

A permutation of length nn is an ordering of the positive integers {1,2,…,n}{1,2,…,n}. For example, π=(5,3,2,1,4)π=(5,3,2,1,4) is a permutation of length 55.

Given: A positive integer n≤7.

Return: The total number of permutations of length n, followed by a list of all such permutations (in any order).

Sample Dataset

3
Sample Output

6
1 2 3
1 3 2
2 1 3
2 3 1
3 1 2
3 2 1

*/
func factorial(n int) int {
	f := 1
	for n > 0 {
		f *= n
		n--
	}
	return f
}

func permutate(list []int, pointer int) {
	if pointer == len(list) {
		for i, n := range list {
			fmt.Print(n)
			if i != len(list) - 1 {
				fmt.Print(" ")
			}
		}
		fmt.Println()
		return
	}

	for i := pointer; i < len(list); i++ {
		permutation := make([]int, len(list))
		copy(permutation, list)
		permutation[pointer] = list[i]
		permutation[i] = list[pointer]
		permutate(permutation, pointer + 1)
	}
}

func main() {
	n := 7
	options := make([]int, 0)
	for i := 1; i <= n; i++ {
		options = append(options, i)
	}
	
	fmt.Println(factorial(n))
	permutate(options, 0)
}