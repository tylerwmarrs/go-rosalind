package main

import (
	"go-rosalind"
	"fmt"
)

/*
Transcription May Begin Anywhereclick to collapse


Figure 1. Schematic image of the particular ORF with start and stop codons shown.
In “Transcribing DNA into RNA”, we discussed the transcription of DNA into RNA, and in “Translating RNA into Protein”, we examined the translation of RNA into a chain of amino acids for the construction of proteins. We can view these two processes as a single step in which we directly translate a DNA string into a protein string, thus calling for a DNA codon table.

However, three immediate wrinkles of complexity arise when we try to pass directly from DNA to proteins. First, not all DNA will be transcribed into RNA: so-called junk DNA appears to have no practical purpose for cellular function. Second, we can begin translation at any position along a strand of RNA, meaning that any substring of a DNA string can serve as a template for translation, as long as it begins with a start codon, ends with a stop codon, and has no other stop codons in the middle. See Figure 1. As a result, the same RNA string can actually be translated in three different ways, depending on how we group triplets of symbols into codons. For example, ...AUGCUGAC... can be translated as ...AUGCUG..., ...UGCUGA..., and ...GCUGAC..., which will typically produce wildly different protein strings.

Problem

Either strand of a DNA double helix can serve as the coding strand for RNA transcription. Hence, a given DNA string implies six total reading frames, or ways in which the same region of DNA can be translated into amino acids: three reading frames result from reading the string itself, whereas three more result from reading its reverse complement.

An open reading frame (ORF) is one which starts from the start codon and ends by stop codon, without any other stop codons in between. Thus, a candidate protein string is derived by translating an open reading frame into amino acids until a stop codon is reached.

Given: A DNA string ss of length at most 1 kbp in FASTA format.

Return: Every distinct candidate protein string that can be translated from ORFs of ss. Strings can be returned in any order.

Sample Dataset

>Rosalind_99
AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCTCTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG
Sample Output

- MLLGSFRLIPKETLIQVAGSSPCNLS
- M
- MGMTPRLGLESLLE
- MTPRLGLESLLE
*/

// We shift the forward strand and reverse strand to obtain the open reading frames.
// One thing to keep in mind is that you MUST keep track of nested open reading frames.

// Here is a manual conversion from DNA to protein. Notice the +1 has a nested ORF.
// +1
//								   M    G   M   T   P   R   L   G   L   E   S   L   L   E   .
// AGC CAT GTA GCT AAC TCA GGT TAC ATG GGG ATG ACC CCG CGA CTT GGA TTA GAG TCT CTT TTG GAA TAA GCC TGA ATG ATC CGA GTA GCA TCT CAG
// MTPRLGLESLLE
// MGMTPRLGLESLLE

// +2
//        M   .
// A GCC ATG TAG CTA ACT CAG GTT ACA TGG GGA TGA CCC CGC GAC TTG GAT TAG AGT CTC TTT TGG AAT AAG CCT GAA TGA TCC GAG TAG CAT CTC AG
// M

// +3
//
// AG CCA TGT AGC TAA CTC AGG TTA CAT GGG GAT GAC CCC GCG ACT TGG ATT AGA GTC TCT TTT GGA ATA AGC CTG AAT GAT CCG AGT AGC ATC TCA G
//

// -1
//                                                                                                                          M
// CTG AGA TGC TAC TCG GAT CAT TCA GGC TTA TTC CAA AAG AGA CTC TAA TCC AAG TCG CGG GGT CAT CCC CAT GTA ACC TGA GTT AGC TAC ATG GCT
// M

// -2
//                                                                                                M   .
// C TGA GAT GCT ACT CGG ATC ATT CAG GCT TAT TCC AAA AGA GAC TCT AAT CCA AGT CGC GGG GTC ATC CCC ATG TAA CCT GAG TTA GCT ACA TGG CT
// M

// -3
//         M   L   L   G   S   F   R   L   I   P   K   E   T   L   I   Q   V   A   G   S   S   P   C   N   L   S   .
// CT GAG ATG CTA CTC GGA TCA TTC AGG CTT ATT CCA AAA GAG ACT CTA ATC CAA GTC GCG GGG TCA TCC CCA TGT AAC CTG AGT TAG CTA CAT GGC T
// MLLGSFRLIPKETLIQVAGSSPCNLS
func main() {
	sequences, err := rosalind.ReadFASTAFile("../data/open_reading_frames/sample.txt")
	if err != nil {
		panic(err)
	}

	dnaSeq := sequences[0].Sequence
	orfs := rosalind.NewStringSet()
	for _, d := range dnaSeq.ORFs() {
		orfs.Add(d.Sequence.ToProtein().String())
	}

	for s, _ := range orfs.Set() {
		fmt.Println(s)
	}
}