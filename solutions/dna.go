package main

import (
	"go-rosalind"
	"fmt"
)


/*
A string is simply an ordered collection of symbols selected from some alphabet and formed into a word; the length of a string is the number of symbols that it contains.

An example of a length 21 DNA string (whose alphabet contains the symbols 'A', 'C', 'G', and 'T') is "ATGCTTCAGAAAGGTCTTACG."

Given: A DNA string ss of length at most 1000 nt.

Return: Four integers (separated by spaces) counting the respective number of times that the symbols 'A', 'C', 'G', and 'T' occur in ss.

Sample Dataset

AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC
Sample Output

20 12 17 21
*/

func main() {	
	sequence, err := rosalind.FileToString("../data/dna/sample.txt")
	if err != nil {
		panic(err)
	}

	counts := rosalind.NucleotideCounts(sequence)
	fmt.Println(counts['A'], counts['C'], counts['G'], counts['T'])
}