package main

import (
	"go-rosalind"
	"fmt"
)

/*

Combing Through the Haystackclick to collapse

Finding the same interval of DNA in the genomes of two different organisms (often taken from different species) is highly suggestive that the interval has the same function in both organisms.

We define a motif as such a commonly shared interval of DNA. A common task in molecular biology is to search an organism's genome for a known motif.

The situation is complicated by the fact that genomes are riddled with intervals of DNA that occur multiple times (possibly with slight modifications), called repeats. These repeats occur far more often than would be dictated by random chance, indicating that genomes are anything but random and in fact illustrate that the language of DNA must be very powerful (compare with the frequent reuse of common words in any human language).

The most common repeat in humans is the Alu repeat, which is approximately 300 bp long and recurs around a million times throughout every human genome (see Figure 1). However, Alu has not been found to serve a positive purpose, and appears in fact to be parasitic: when a new Alu repeat is inserted into a genome, it frequently causes genetic disorders.

Problem

Given two strings ss and tt, tt is a substring of ss if tt is contained as a contiguous collection of symbols in ss (as a result, tt must be no longer than ss).

The position of a symbol in a string is the total number of symbols found to its left, including itself (e.g., the positions of all occurrences of 'U' in "AUGCUUCAGAAAGGUCUUACG" are 2, 5, 6, 15, 17, and 18). The symbol at position ii of ss is denoted by s[i]s[i].

A substring of ss can be represented as s[j:k]s[j:k], where jj and kk represent the starting and ending positions of the substring in ss; for example, if ss = "AUGCUUCAGAAAGGUCUUACG", then s[2:5]s[2:5] = "UGCU".

The location of a substring s[j:k]s[j:k] is its beginning position jj; note that tt will have multiple locations in ss if it occurs more than once as a substring of ss (see the Sample below).

Given: Two DNA strings ss and tt (each of length at most 1 kbp).

Return: All locations of tt as a substring of ss.

Sample Dataset

GATATATGCATATACTT
ATAT
Sample Output

2 4 10

*/

func main() {
	sequences, err := rosalind.FileToSlice("../data/find_motif_dna/sample.txt")
	if err != nil {
		panic(err)
	}

	seqA := rosalind.DNASequence(sequences[0])
	seqB := rosalind.DNASequence(sequences[1])
	positions, err := rosalind.FindMotifPositions(seqA, seqB)
	if err != nil {
		panic(err)
	}
	
	for i, position := range positions {
		fmt.Print(position)

		if len(positions) > 1 && len(positions) != i+1 {
			fmt.Print(" ")
		}

		if len(positions) == i + 1 {
			fmt.Println()
		}
	}
}