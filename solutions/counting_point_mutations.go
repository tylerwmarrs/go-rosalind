package main

import (
	"go-rosalind"
	"fmt"
)

/*

Problem


Figure 2. The Hamming distance between these two strings is 7. Mismatched symbols are colored red.
Given two strings ss and tt of equal length, the Hamming distance between ss and tt, denoted dH(s,t)dH(s,t), is the number of corresponding symbols that differ in ss and tt. See Figure 2.

Given: Two DNA strings ss and tt of equal length (not exceeding 1 kbp).

Return: The Hamming distance dH(s,t)dH(s,t).

Sample Dataset

GAGCCTACTAACGGGAT
CATCGTAATGACGGCCT
Sample Output

7

*/

func main() {
	lines, err := rosalind.FileToSlice("../data/counting_point_mutations/sample.txt")
	if err != nil {
		panic(err)
	}

	dna := rosalind.DNASequence(lines[0])
	dna2 := rosalind.DNASequence(lines[1])

	hammingDistance, err := rosalind.HammingDistance(dna, dna2)
	if err != nil {
		panic(err)
	}
	fmt.Println(hammingDistance)
}