package main

import (
	"go-rosalind"
	"fmt"
)

/*

Problem

In a weighted alphabet, every symbol is assigned a positive real number called a weight. A string formed from a weighted alphabet is called a weighted string, and its weight is equal to the sum of the weights of its symbols.

The standard weight assigned to each member of the 20-symbol amino acid alphabet is the monoisotopic mass of the corresponding amino acid.

Given: A protein string PP of length at most 1000 aa.

Return: The total weight of PP. Consult the monoisotopic mass table.

Sample Dataset

SKADYEK
Sample Output

821.392

*/

func main() {
	sequence, err := rosalind.FileToString("../data/calculating_protein_mass/sample.txt")
	if err != nil {
		panic(err)
	}

	proteinSequence := rosalind.ProteinSequence(sequence)
	fmt.Println(proteinSequence.Mass())
}