package rosalind

import (
	"fmt"
	"unicode"
)

type Nucleotide rune

func (n *Nucleotide) String() string {
	return string(*n)
}

func (n *Nucleotide) Rune() rune {
	return rune(*n)
}

func (n *Nucleotide) ToString() string {
	return fmt.Sprintf("%c", *n)
}

func (n *Nucleotide) Complement() Nucleotide {
	switch *n {
		case 'A': return Nucleotide('T')
		case 'C': return Nucleotide('G')
		case 'T': return Nucleotide('A')
		case 'G': return Nucleotide('C')
		default: return Nucleotide(' ')
	}
}

func (n *Nucleotide) Meaning() string {
	switch *n {
		case 'A': return "Adenine"
		case 'C': return "Cytosine"
		case 'T': return "Thymine"
		case 'G': return "Guanine"
		case 'U': return "Uracil"
		default: return "Not a nucleotide!"
	}
}

func RNANucleotides() []rune {
	return []rune {'A', 'C', 'G', 'U'}
}

func DNANucleotides() []rune {
	return []rune {'A', 'C', 'G', 'T'}
}

func IsStringNucleotide(s string) bool {
	tooLong := len(s) == 1
	return tooLong && IsRuneNucleotide([]rune(s)[0])
}

func IsRuneNucleotide(r rune) bool {
	r = unicode.ToUpper(r)
	return r == 'A' || r == 'C' || r == 'T' || r == 'G' || r == 'U'
}

func NucleotideCounts(s string) map[Nucleotide]int {
	counts := make(map[Nucleotide]int)
	for _, r := range s {
		if IsRuneNucleotide(r) {
			counts[Nucleotide(r)]++
		}
	}
	
	return counts
}