package rosalind

import (
  "bufio"
  "os"
  "strings"
)

type FASTASequence struct {
	ID string
	Sequence DNASequence
}

func ReadFASTAFile(filePath string) ([]FASTASequence, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	seqs := make([]FASTASequence, 0)
	var seq FASTASequence
	var seqPieces []string

	for scanner.Scan() {
        line := strings.TrimSpace(scanner.Text())
        if (strings.HasPrefix(line, ">")) {
        	if (len(seqPieces) > 0) {
        		seq.Sequence = DNASequence(strings.Join(seqPieces, ""))
        		seqs = append(seqs, seq)        		
        	}

        	seq = FASTASequence{strings.Replace(line, ">", "", 1), ""}
        	seqPieces = make([]string, 0)
        } else {
        	seqPieces = append(seqPieces, line)
        }
    }

    if (len(seqPieces) > 0) {
		seq.Sequence = DNASequence(strings.Join(seqPieces, ""))
		seqs = append(seqs, seq)        		
	}

    // check for errors
    if err = scanner.Err(); err != nil {
      return nil, err
    }

    return seqs, err
}