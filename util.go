package rosalind

import (
	"io/ioutil"
	"strings"
	"os"
	"bufio"
)

// Helper to read an entire file into a string
func FileToString(filePath string) (string, error) {
	data, err := ioutil.ReadFile(filePath)
	return string(data), err
}

// Helper to read file lines into slice
func FileToSlice(filePath string) ([]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	lines := make([]string, 0)

	for scanner.Scan() {
        lines = append(lines, strings.TrimSpace(scanner.Text()))
    }

    // check for errors
    if err = scanner.Err(); err != nil {
      return nil, err
    }

    return lines, err
}