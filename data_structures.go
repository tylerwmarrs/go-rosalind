package rosalind

// Simple data structure to mimic a String Set
type StringSet struct {
    set map[string]bool
}

func NewStringSet() *StringSet {
	s := &StringSet{}
	s.set = make(map[string]bool)
	return s
}

func (set *StringSet) Add(s string) bool {
    _, found := set.set[s]
    set.set[s] = true
    return !found   //False if it existed already
}

func (set *StringSet) Set() map[string]bool {
	return set.set
}