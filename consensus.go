package rosalind

import (
	"bytes"
	"fmt"
)

type ConsensusProfile map[string]map[int]int

func (c *ConsensusProfile) PrintTable() {
	length := c.Length()
	for k, v := range *c {
		fmt.Print(fmt.Sprintf("%s: ", k))
		for i := 0; i < length; i++ {
			if c, ok := v[i]; ok {
				fmt.Print(c)				
			} else {
				fmt.Print("0")
			}
			fmt.Print(" ")
		}
		fmt.Println()
	}
}

func (c *ConsensusProfile) Strings() []string {
	return nil
}

func (c *ConsensusProfile) String() string {
	length := c.Length()
	maxCount := 0
	currentChar := ""
	var seq bytes.Buffer

	for i := 0; i < length; i++ {
		maxCount = 0
		currentChar = ""
		for k, v := range *c {
			if count, ok := v[i]; (ok && count > maxCount) || maxCount == 0 {
				currentChar = k
				maxCount = count
			}
		}
		seq.WriteString(currentChar)
	}
	return seq.String()
}

func (c *ConsensusProfile) Length() int {
	length := 0
	for _, v := range *c {
		for l, _ := range v {
			if l > length {
				length = l
			}
		}
	}
	return length + 1
}

func CombinedConsensusProfile(seqs []Sequence) ConsensusProfile {
	consensus := make(map[string]map[int]int)

	for _, seq := range seqs {
		prof := seq.ConsensusProfile()
		for k, pos := range prof {
			if _, ok := consensus[k]; !ok {
				consensus[k] = make(map[int]int)
			}

			for i, c := range pos {
				consensus[k][i] += c
			}
		}
	}

	return consensus
}